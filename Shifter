---------------------------------------------------------------------------------------------
-- FILE             : SHIFTER.vhd 
-- PROJECT          : Lab 5 - SHIFTER
-- PROGRAMMER       : Ankit Guhe  	  
-- FIRST VERSION    : 2017, Feb 14
-- COPYRIGHT 		  : 2017, EECE8020 Reconfigurable system Principles. All rights reserved.
--
-- DESCRIPTION      : * This is an RTL design for an 8 bit shift register
--                    * There is a single 8 bit data input F which is loaded.
--							 * The shift register performs various shift operations on the input
--                      data F based on the shift select function HSEL.
--                    * An optional Carry input CI could be loaded for rotate with carry. 
--                    * A Carry output flag monitors the the MSB bit or the LSB bit based 
--							   on the rotate operation selected. MSB bit is monitored for rotate 
--                      left with carry and LSB bit is monitored for rotate right with 
--                      carry operations respectively.
--  
-- REVISION HISTORY : 02/14/2017 by Ankit Guhe Rev 1.0             
----------------------------------------------------------------------------------------------

-- Standard Library Files
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


-- Entity Declaration
entity SHIFTER is
	port(
	F		:	in		unsigned (7 downto 0);					-- F input bus
	CI		:	in 	std_logic:='0';							-- Carry input
	HSEL	:	in		unsigned (2 downto 0);					-- Shift Function Select
	S		:	out	unsigned (7 downto 0);					-- Shifter Output
	CO		:	out	std_logic									-- Carry output
	);
end SHIFTER;


-- Architecture Definition for the Entity 
architecture rtl of SHIFTER is 

signal temp	: unsigned (8 downto 0); 						-- signal declaration

	begin
	
	process(HSEL,F,CI)
		begin
		
		case HSEL is
		when "000" 	=> S  <= F;									-- No Shift
		               CO <= CI;
							
		when "001"	=> S 	<= F sll 1;							-- Shift left
							CO <= CI;
							
		when "010" 	=> S <= F srl 1;							-- Shift right
							CO <= CI;
							
		when "011" 	=> S <= "00000000";						-- All Zero's in output
							CO <= CI;
							
		when "100" 	=> temp <= (F&CI) rol 1;				-- Rotate left with carry
							S    <= temp(8 downto 1);
							CO   <= std_logic(temp(0));
		
		
		when "101" 	=> S <= F rol 1;							-- Rotate left
							CO <= CI;
						
		when "110" 	=> S <= F ror 1;							-- Rotate right 
							CO <= CI;
							
		when "111" 	=> temp <= (CI&F) ror 1;				-- Rotate right with carry
							S    <= temp(7 downto 0);
							CO   <= std_logic(temp(8));				
							
		when others => S <= "00000000";						-- default case
							CO <= CI;
		end case;
		
	end process;
end architecture rtl;