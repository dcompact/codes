---------------------------------------------------------------------------------------------
-- FILE             : ALU.vhd 
-- PROJECT          : Lab 4 - ALU
-- PROGRAMMER       : Ankit Guhe, Shivani Joshi, Manjot Hansra  		  
-- FIRST VERSION    : 2017, Jan 31
-- COPYRIGHT 		  : 2017, EECE8020 Reconfigurable system Principles. All rights reserved.
--
-- DESCRIPTION      : * This is an RTL design for a 4 bit ALU. It processes 4 bit quantities.
--                    * There are two Data inputs( A and B), each 4 bits wide.
--							 * The ALU output is 4 bits wide and there are Status Flags to check 
--                      the Carry, Overflow, Sign, and Zeroes for the output resulting from
--                      any arithmetic or logical operations.
--                    * The operation select(SSEL) input(4 bits wide) is used to select the 
--							   desired arithmetic or logical operation on the input data A and B.
--  
-- REVISION HISTORY : 01/31/2017 by Ankit Guhe Rev 1.0             
----------------------------------------------------------------------------------------------


-- Standard Library Files
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


-- Entity Declaration
entity ALU is
	port(
	SSEL	:	in		unsigned (3 downto 0);	-- function select
	A		:	in		unsigned (7 downto 0);	-- A input bus
	B		:	in		unsigned (7 downto 0);	-- B input bus
	F		:	inout	unsigned (7 downto 0);	-- function output
	Z		:	out	std_logic;					-- Zero flag						
	S		:	out	std_logic;					-- Sign bit flag
	C		:	out	std_logic;					-- Carry out flag
	V		:	out	std_logic					-- Overflow flag
	);
end ALU;


-- Architecture Definition for the Entity  
architecture rtl of ALU is 

signal temp: unsigned (8 downto 0); 	-- signal declaration


-- function to set/reset zero flag of the ALU
function check_zeroFlag (inp : unsigned (7 downto 0))
								return std_logic is
	begin
	if(inp = "00000000") then
    return '1';
	else 
    return '0';
   end if;
end check_zeroFlag;			


-- function to set/reset sign flag of the ALU
function check_signFlag (inp : std_logic)
								 return std_logic is
	begin
	if(inp = '1') then
    return '1';
	else 
    return '0';
   end if;
end check_signFlag;	


	begin
	
	-- Process block 
	process(SSEL,A,B)
		begin

		-- initialize all status flags of the ALU to '0'
		Z <= '0';
		S <= '0';
		C <= '0';
		V <= '0';
		
		case SSEL is
		
			when "0000" =>									
							F <= A;							-- Transfer A
							Z <= check_zeroFlag(F);
						   S <= check_signFlag(std_logic(F(7)));	
							
			when "0001" =>									
							temp <= ("0" & A) + 1;		-- increment A by 1, concatenate A with 0 to match the output size
							F	  <= temp(7 downto 0);  
							C	  <= temp(8);
							V	  <= temp(8); 
							Z <= check_zeroFlag(F);
						   S <= check_signFlag(std_logic(F(7)));	
			
			when "0010" =>
							temp <= ("0" & A) + B;		-- Add A+B, concatenate A with 0 to match the output size
							F	  <= temp(7 downto 0);
							C	  <= temp(8);
							V	  <= temp(8); 
							Z <= check_zeroFlag(F);
						   S <= check_signFlag(std_logic(F(7)));	
							
			when "0101" =>
							F	 <= A - B;					-- Subtract A-B
							Z <= check_zeroFlag(F);
						   S <= check_signFlag(std_logic(F(7)));	
							
			when "0110" =>
							F	 <= A - 1;					-- Decrement A by 1
							Z <= check_zeroFlag(F);
						   S <= check_signFlag(std_logic(F(7)));	
			
			when "0111" =>
							F	<= A;							-- Transfer A and Carry = 0
							C	<= '0';
							Z <= check_zeroFlag(F);
						   S <= check_signFlag(std_logic(F(7)));	
							
			when "1000" => 
							F  <= A and B;					-- A and B, logical and operation
			
			when "1010" =>
							F  <= A or B;					-- A or B, logical or operation
							
         when "1100" =>
							F  <= A xor B;					-- A xor B, logical xor operation
							
			when "1110" =>
							F  <= not A;					-- not of A, complement A 
		
		   when others =>									-- default case
							F  <= "00000000";
							
		end case;
	end process;
end architecture rtl;	
							